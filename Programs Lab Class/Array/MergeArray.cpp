#include <iostream>
using namespace std;
void merge(int arr[], int LB, int UB, int key) {
  int p = UB - LB + 1;
  int q = key - LB;

  int A[p], B[q];

  for (int i = 0; i < p; i++)
    A[i] = arr[LB + i];
  for (int j = 0; j < q; j++)
    B[j] = arr[UB + 1 + j];

  int i, j, k;
  i = 0;
  j = 0;
  k = LB;

  while (i < p && j < q) {
    if (A[i] <= B[j]) {
      arr[k] = A[i];
      i++;
    } else {
      arr[k] = B[j];
      j++;
    }
    k++;
  }

  while (i < p) {
    arr[k] = A[i];
    i++;
    k++;
  }

  while (j < q) {
    arr[k] = B[j];
    j++;
    k++;
  }
}

void mergeSort(int arr[], int l, int r) {
  if (l < r) {
    int m = (l + r) / 2;
    mergeSort(arr, l, m);
    mergeSort(arr, m + 1, r);
    merge(arr, l, m, r);
  }
}

void printArray(int arr[], int size) {
  for (int i = 0; i < size; ++i)
    cout << arr[i] << " ";
  cout << endl;
}

int main() {
  int arr1[] = {1, 3, 4, 9,10};
  int arr2[] = {2, 5, 6, 7, 8, 11};
  int size1 = sizeof(arr1) / sizeof(arr1[0]);
  int size2 = sizeof(arr2) / sizeof(arr2[0]);
  int arr[size1 + size2];
  for (int i = 0; i < size1; i++)
    arr[i] = arr1[i];
  for (int i = 0; i < size2; i++)
    arr[size1 + i] = arr2[i];
  int size = size1 + size2;
  mergeSort(arr, 0, size - 1);
  cout << "Sorted array: \n";
  printArray(arr, size);
  return 0;
}
